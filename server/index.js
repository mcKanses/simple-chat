const express = require('express')
  , fs = require('fs')
  , http = require('http')
  , socketio = require('socket.io')
  , uuid = require('uuid');

const app = express()
  , server = http.Server(app)
  , io = socketio(server);

const chat = {
  users: []
};

app.use(express.static('./public'));

app.use(express.json());

app.post('/login', async (req, res, next) => {
  try {
    const users = chat.users;

    // check if user already in db
    const userInDB = users.filter(({ nickname }) => req.body.nickname === nickname)?.[0];

    if (!!userInDB?.id && userInDB.id !== req.body.id) return res.status(409).send('username already in use');

    const id = uuid.v1();

    chat.users = [
      ...(users.filter(user => user.nickname !== req.body.nickname)),
      {
        id,
        nickname: req.body.nickname,
        loginTimestamp: Date.now()
      }
    ];

    // fs.writeFileSync('chat.db.json', JSON.stringify(chat, null, 2));

    res.json({ id });

    console.log(chat);
    // chatData = { ...chat };

  } catch (err) {
    console.error(err.message);
  }
});

app.post('/logout', async (req, res, next) => {
  try {
    // const chat = fs.readFileSync('chat.db.json', 'utf8');
    chat.users = chat.users.filter(user => user.id !== req.body.id);
    console.log(chat);
    res.send('ok');

  } catch (err) {
    
  }
})

io.on('connection', socket => {
  console.log('a user connected');

  socket.on('chat message', data => {
    console.log(data.msg);
    console.log(chat.users);
    const { id, msg } = data;
    console.log(data);
    const sender = chat.users.filter(user => user.id === id)?.[0];
    console.log(sender);
    io.emit('chat message', {
      sender,
      msg
    });
  })
});




server.listen(8000, () => console.log('app listening on port 8000'));

