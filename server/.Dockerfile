FROM node:15.11.0-alpine3.13

RUN apk update

RUN mkdir /usr/bin/app

WORKDIR /usr/bin/app

COPY server/package.json .

RUN npm install

RUN ls

CMD [ "npm", "start" ]