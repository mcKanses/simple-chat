if (!sessionStorage.getItem("id")) window.location = "/";

window.onload = (ev) => {
  const socket = io('ws://localhost:8000');

  const divMessages = document.querySelector('#messages');

  const linkLogout = document.querySelector('a[href="/logout"]');

  const frmNewMessage = document.querySelector('#form-new-message');
  const inputNewMessage = document.querySelector("#input-new-message");
  const btnSubmit = document.querySelector("input[type=submit]");

  linkLogout.addEventListener('click', async ev => {
    ev.preventDefault();
    try {
      const response = await axios.post('/logout', {
        id: sessionStorage.getItem('id')
      });

      sessionStorage.removeItem('id');
  
      window.location = '/';
    } catch (err) {
      console.error(err.message);
    }
  });

  frmNewMessage.addEventListener('submit', async (ev) => {
    console.log(socket.id);
    ev.preventDefault();
    try {
      socket.emit("chat message", {
        id: sessionStorage.getItem('id'),
        msg: inputNewMessage.value
      });
      inputNewMessage.value = "";
      inputNewMessage.focus();
    } catch (err) {
      console.error(err.message);
    }
  });

  socket.on('chat message', data => {
console.log(data);
    const divMessage = document.createElement('div');
    divMessage.classList.add('message');
    divMessage.classList.add(data.sender.id === sessionStorage.getItem('id') ? 'out': 'in');
    divMessage.innerText = `${data.sender.nickname}: ${data.msg}`;

    divMessages.appendChild(divMessage);

  });

};