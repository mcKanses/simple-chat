window.onload = ev => {
  // console.log(io);
  const frmLogin = document.querySelector("form");
  const inputNickname = frmLogin.querySelector("#nickname");
  const btnLogin = frmLogin.querySelector("input[type=submit]");

  inputNickname.removeAttribute("disabled");

  inputNickname.addEventListener("input", (ev) => btnLogin[ev.target.value?.length > 0 ? "removeAttribute" : "setAttribute"]("disabled", null));

  inputNickname.focus();

  btnLogin.addEventListener("click", async (ev) => {
    ev.preventDefault();
    try {
      const response = await axios.post("/login", {
        nickname: inputNickname.value,
        id: sessionStorage.getItem('id')
      });

      if (response.statusText === 'OK') {
        sessionStorage.setItem('id', response.data.id);
        window.location = '/chat.html';
      }
    } catch (err) {
      if (err.isAxiosError) {
        switch (err.response?.data) {
          case "username already in use":
            alert('Username wird bereits benutzt');
            break;
          default:
            alert(err.data);
            break;
        }
      }
    }
  });
};